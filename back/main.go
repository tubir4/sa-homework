package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sa-homework/back/lib"
	"strconv"
	"time"
)

func main() {
	reminder := false
	tasks := []lib.Task{
		{
			ID: 0,
			Text: "First",
			Day: time.Now(),
			Reminder: &reminder,
		},
		{
			ID: 1,
			Text: "Second",
			Day: time.Now(),
			Reminder: &reminder,
		},
		{
			ID: 0,
			Text: "Third",
			Day: time.Now(),
			Reminder: &reminder,
		},
	}
	r := gin.Default()
	r.GET("/health", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"status": "ok",
		})
	})
	r.GET("/tasks", func(c *gin.Context) {
		c.JSON(200, tasks)
	})
	r.GET("/tasks/:id", func(c *gin.Context) {
		idParam := c.Param("id")
		id, err := strconv.Atoi(idParam)
		if err != nil {
			c.JSON(400, "id isn't integer")
			return
		}
		c.JSON(200, tasks[id])
	})
	r.POST("/tasks", func(c *gin.Context) {
		var task lib.Task
		if err := c.ShouldBindJSON(&task); err != nil {
			c.JSON(400, err)
			return
		}
		tasks = append(tasks, task)
		c.JSON(200, task)
	})
	r.PUT("/tasks/:id", func(c *gin.Context) {
		var task lib.Task
		if err := c.ShouldBindJSON(&task); err != nil {
			c.JSON(400, err)
			return
		}
		idParam := c.Param("id")
		id, err := strconv.Atoi(idParam)
		if err != nil {
			c.JSON(400, "id isn't integer")
			return
		}
		task.ID = id
		for _, t := range tasks {
			if t.ID == id {
				t = task
				break
			}
		}
		c.JSON(200, task)
	})
	r.DELETE("/tasks/:id", func(c *gin.Context) {
		idParam := c.Param("id")
		id, err := strconv.Atoi(idParam)
		if err != nil {
			c.JSON(400, "id isn't integer")
			return
		}
		task := lib.Task{}
		for i, t := range tasks {
			if t.ID == id {
				task = t
				tasks = append(tasks[:i], tasks[i+1:]...)
				break
			}
		}
		c.JSON(200, task)
	})
	r.Run()
}
