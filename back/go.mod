module sa-homework

go 1.16

require (
	github.com/gin-gonic/gin v1.7.1
	gitlab.com/sa-homework/back v0.0.0-00010101000000-000000000000
)

replace gitlab.com/sa-homework/back => ./../back
