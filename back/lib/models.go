package lib

import "time"

type Task struct {
	ID       int       `json:"id"`
	Text     string    `json:"text"`
	Reminder *bool     `json:"reminder"`
	Day      time.Time `json:"time"`
}
