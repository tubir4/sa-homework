
#How to run

- kubectl apply -f back.yml
- kubectl apply -f front.yml
- kubectl port-forward service/sa-homework-back 8000:8080
- kubectl port-forward service/sa-homework-front 3000:3000